# Securepay

https://www.securepay.com.au/
####
Note: More features are comming.
## Installation
NPM: 
```bash
npm install securepay
```
Yarn: 
```bash
yarn add securepay
```

## Server Usage
Paypal Example: 

```typescript
import { Paypal, Alipay, WechatPay } from 'securepay';
import { PaymentChanel } from 'securepay/dist/enums'
import { PaymentMethods } from 'securepay/dist/enums/payment-methods.enum';

/**
 * Paypal Example 
 */
const paypalClient = new Paypal({
  sandbox     : !process.env.PRODUCTION,
  debugLevel  : Number(process.env.DEBUG_LEVEL),
  clientId    : process.env.SECUREPAY_CLIENT_ID,
  clientSecret: process.env.SECUREPAY_CLIENT_SECRET,
  merchantCode: process.env.SECUREPAY_MERCHANT_CODE
});
const paypalResponse = await paypalClient.initialTransaction({
  orderId     : orderId,
  amount      : 1000,
  ip          : payload.ip,
  redirectUrls: {
    successUrl: "https://your-succeed-domain",
    cancelUrl : "https://your-canceled-domain"
  }
});

/** 
 * Alipay Example
 */
const client = new Alipay({
  sandbox     : !process.env.PRODUCTION,
  debugLevel  : Number(process.env.DEBUG_LEVEL),
  clientId    : process.env.SECUREPAY_CLIENT_ID,
  clientSecret: process.env.SECUREPAY_CLIENT_SECRET,
  merchantCode: process.env.SECUREPAY_MERCHANT_CODE
});
const paypalResponse = await client.initialTransaction({
  orderId          : orderId,
  amount           : 1000,
  ip               : payload.ip,
  orderDescription : orderId,
  paymentChannel   : PaymentChanel.IN_BROWSER,
  redirectUrls     : {
    successUrl: "https://your-succeed-domain"
  }
});
```

## Client Usage

```HTML
<div id="securepay-ui-container" class="w-100 mx-auto text-center">
```
```typescript
import { CardPaymentClient, CardPaymentClientCallbackEvents, CardPaymentInstrumentRequest } from "securepay";

  /**
   * Initial Card Payment UI
   *
   * @returns void
   */
  ngAfterViewInit(): void {
    this.cardPaymentClient = new CardPaymentClient({
      sandbox     : !environment.production,
      clientId    : environment.SECUREPAY_CLIENT_ID,
      merchantCode: environment.SECUREPAY_MERCHANT_CODE,
      containerId : "securepay-ui-container",
      callback    : this.eventCallBack()
    })
  }

  /**
   * Listen to callback
   * 
   * @returns void
   */
  eventCallBack() {
    const that = this;
    return (event: CardPaymentClientCallbackEvents, res: CardPaymentInstrumentRequest) => {
      if (event == CardPaymentClientCallbackEvents.ON_LOAD_COMPLETE)
        that.securepayUiLoaded = true;
      else if (event == CardPaymentClientCallbackEvents.ON_TOKENISE_SUCCESS)
        console.info(res);
    }
  }

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)