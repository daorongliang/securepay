export enum JsSdkUrl {
  UI = "https://payments.auspost.net.au/v3/ui/client/securepay-ui.min.js",
  SANDBOX_UI = "https://payments-stest.npe.auspost.zone/v3/ui/client/securepay-ui.min.js"
}