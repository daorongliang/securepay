export enum WechatPayTransactionStatus {
  /** 
   * The payment has been successful.
   */
  PAID = 'paid',

  /**
   * The payment has been failed.
   */
  FAILED = 'failed',

  /**
   * The transaction is in pending state or it has not terminated, e.g. waiting on customer authorization
   */
  INPROGRESS = 'inprogress',

  /**
   * The result of execution is not determined(e.g. due to connectivity issues). To verify the status in this case use retrieve WechatPay get order details endpoint.
   */
  UNKNOWN = 'unknown',

  /**
   * The payment has been cancelled
   */
  CANCELLED = 'cancelled'

}