export enum AuthenticationGrantType {
  DEFAULT            = "client_credentials",
  CLIENT_CREDENTIALS = "client_credentials"
}