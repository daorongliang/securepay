export enum PaymentStatus {
  PAID = 'paid',
  SUCCESS = 'success',
  FAILED = 'failed',
  UNKNOWN = 'unknown'
}