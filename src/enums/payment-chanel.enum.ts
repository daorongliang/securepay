export enum PaymentChanel {
  /**
   * InBrowser is used if customer is initiating the transaction from browser
   */
  IN_BROWSER = 'InBrowser', 

  /**
   * InApp is used if transaction is initiated from within Alipay mobile app
   */

  IN_APP = 'InApp', 

  /**
   * InstoreMerchantQRCode is used if transaction is initiated from merchant terminal
   */
  INSTORE_CUSTOMER_QR_CODE = 'InstoreCustomerQRCode',

  /**
   * InstoreCustomerQRCode is used if transaction is initiated using customer's Alipay app QRCode
   */
  INSTORE_MERCHANT_QR_CODE = 'InstoreMerchantQRCode'
}