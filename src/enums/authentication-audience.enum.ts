export enum AuthenticationAudience {
  DEFAULT = "https://api.payments.auspost.com.au",
  AUSPOST = "https://api.payments.auspost.com.au"
}