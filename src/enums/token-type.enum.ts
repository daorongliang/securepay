export enum TokenType {
  SECUREPAY_JWT = 'Securepay JWT Auth',
  SECUREPAY_BASIC = 'Securepay Basic Auth'
}