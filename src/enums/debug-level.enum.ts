export enum DebugLevel {
  NONE     = 0,
  ERROR    = 1,
  WARNING  = 2,
  ALL      = 3
}