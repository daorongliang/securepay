export enum CardPaymentClientCallbackEvents {
  ON_TOKENISE_SUCCESS     = "onTokeniseSuccess",
  ON_LOAD_COMPLETE        = "onLoadComplete",
  ON_TOKENISE_ERROR       = "onTokeniseError",
  ON_FORM_VALIDITY_CHANGE = "onFormValidityChange",
  ON_BIN_CHANGE           = "onBINChange",
  ON_CARD_TYPE_CHANGE     = "onCardTypeChange"
}