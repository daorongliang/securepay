export enum PaymentMethods {
  PAYPAL       = "Paypal",
  CARD_PAYMENT = "Card Payment",
  APPLE_PAY    = "Apple Pay",
  WECHAT_PAY   = "Wechat Pay",
  ALIPAY       = 'Alipay'
}