export enum Endpoints {
  /**
   * SecurePay API uses the OAuth 2.0 protocol for authentication and authorization
   */
  AUTHENTICATION = "https://welcome.api2.auspost.com.au/oauth/token",

  /**
   * BASE URL
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-environment-details
   */
  BASE_URL = "https://payments.auspost.net.au"
}