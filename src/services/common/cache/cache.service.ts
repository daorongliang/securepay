import * as cacheManager from "cache-manager";

export class CacheService {
  
  private _cache: cacheManager.Cache;

  constructor() {
    this._cache = cacheManager.caching({
      store: "memory",
      ttl: 86000
    })
  }

  /**
   * Get Cached Value
   */
  async get(key: string) {
    const value: string | undefined = await this._cache.get(key);
    if (value) {
      return JSON.parse(value)?.value;
    }
    return undefined;
  }

  /**
   * Set Cache
   * 
   * @returns 
   */
  async set(key: string, data: any, options?: cacheManager.CachingConfig) {
    return this._cache.set(key, JSON.stringify({
      value: data
    }), options);
  }
}