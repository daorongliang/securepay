import { TokenType } from './../../../enums/token-type.enum';
import axios, { AxiosRequestHeaders } from "axios";
import { AuthenticationService } from '../../authentication/authentication.service';
import { SecurepayConstruction } from '../../../interfaces/common/construction.interface';

export class RequestService {

  private _auth: AuthenticationService;

  constructor(options: SecurepayConstruction) {
    this._auth = new AuthenticationService(options);
  }

  /**
   * Get Token Header
   */
  async getToken(token_type: TokenType) {
    if (token_type == TokenType.SECUREPAY_JWT) {
      return (await this._auth.getJwtToken());
    }
    else return "";
  }

  /**
   * Get request
   */
  async get(options: {
    url: string,
    token_type?: TokenType
  }): Promise<any> {
    /** Get Authentication */
    const headers = {} as AxiosRequestHeaders;
    if (options.token_type) {
      headers.authorization = (await this.getToken(options.token_type))
    }
    const { data } = await axios.get(options.url, {
      headers
    }).catch(error => {
      throw error.response.data;
    });

    return data;
  }

  /**
   * Get request
   */
  async post(options: {
    url: string,
    data?: any,
    token_type?: TokenType
  }): Promise<any> {
    /** Get Authentication */
    const headers = {} as AxiosRequestHeaders;
    if (options.token_type) {
      headers.authorization = (await this.getToken(options.token_type))
    }

    const { data } = await axios({
      method: 'POST',
      url: options.url, 
      data: options.data, 
      headers
    }).catch(error => {
      throw error.response.data;
    });

    return data;
  }
}