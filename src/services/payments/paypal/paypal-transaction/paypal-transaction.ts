import { PaypalEndpoints } from "../../../../constants/paypal-endpoints.const";
import { TokenType, DebugLevel } from "../../../../enums";
import { SecurepayConstruction } from "../../../../interfaces/common/construction.interface";
import { PaypalInitialResponse } from "../../../../interfaces/paypal/paypal-initial-response.interface";
import { PaypalObject } from "../../../../interfaces/paypal/paypal-object.interface";
import { PaypalTransactionExecute } from "../../../../interfaces/paypal/paypal-transaction-execute.interface";
import { PaypalTransactionInitial } from "../../../../interfaces/paypal/paypal-transaction-initial.interface";
import { PaypalTransactionRefund } from "../../../../interfaces/paypal/paypal-transaction-refund.interface";
import { RequestService } from "../../../common/request/request.service";

export class PaypalTransactionService {

  /** Services */
  private _http: RequestService;

  /** Variables */
  private sandbox      : boolean;
  private debugLevel   : DebugLevel;
  private clientId     : string;
  private clientSecret : string;
  private merchantCode : string;
  
  constructor(options: SecurepayConstruction) {
    this._http        = new RequestService(options);
    this.sandbox      = options.sandbox    || false;
    this.debugLevel   = options.debugLevel || DebugLevel.NONE;
    this.clientId     = options.clientId;
    this.clientSecret = options.clientSecret;
    this.merchantCode = options.merchantCode;
  }

  /**
   * Initiates a PayPal express checkout transaction.
   * 
   * @param {PaypalTransactionInitial} payload
   */
  initialTransaction(payload: PaypalTransactionInitial): Promise<PaypalInitialResponse> {
    if (!payload.merchantCode)
      payload.merchantCode = this.merchantCode;
    return this._http.post({
      url: PaypalEndpoints.INITIAL(this.sandbox),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }

  /**
   * Executes a PayPal express checkout transaction after the customer has logged into PayPal and accepted it.
   * 
   * @param {string} orderId
   * @param {PaypalTransactionExecute} payload
   */
  executeTransaction(orderId: string, payload: PaypalTransactionExecute) {
    if (!payload.merchantCode)
      payload.merchantCode = this.merchantCode;
    return this._http.post({
      url: PaypalEndpoints.EXECUTE(this.sandbox, orderId),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }

  /**
   * Refunds a previously executed PayPal transaction
   * 
   * @param {string} orderId
   * @param {PaypalTransactionRefund} payload
   */
  refundTransaction(orderId: string, payload: PaypalTransactionRefund) {
    if (!payload.merchantCode)
      payload.merchantCode = this.merchantCode;
    return this._http.post({
      url: PaypalEndpoints.REFUND(this.sandbox, orderId),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }

  /**
   * Retrieves billing & shipping details for a customer that has previously initiated/executed a PayPal transaction
   * 
   * @param {string} orderId
   * @param {string} merchantCode
   */
  retrieveTransaction(orderId: string, merchantCode?: string): Promise<PaypalObject> {
    if (!merchantCode)
        merchantCode = this.merchantCode;
    return this._http.get({
      url: PaypalEndpoints.RETRIEVE(this.sandbox, orderId, merchantCode),
      token_type: TokenType.SECUREPAY_JWT
    })
  }
}