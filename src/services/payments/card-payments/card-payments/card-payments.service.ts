import { CardPaymentEndpoints } from './../../../../constants/card-payment-endpoints.const';
import { TokenType, DebugLevel } from '../../../../enums';
import { CardPaymentCreate } from '../../../../interfaces/card-payment/card-payment-create.interface';
import { RequestService } from '../../../common/request/request.service';
import { CardPaymentObject } from '../../../../interfaces/index';
import { SecurepayConstruction } from '../../../../interfaces/common/construction.interface';

export class CardPaymentsService {
  
  /** Services */
  private _http: RequestService;

  private sandbox: boolean;
  private debugLevel: DebugLevel;

  constructor(options: SecurepayConstruction) {
    this._http = new RequestService(options);
    this.sandbox = options.sandbox || false;
    this.debugLevel = options.debugLevel || DebugLevel.NONE;
  }
  
  /**
   * Create card payment, documentation:
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-create-payment
   * 
   * @param payload CardPaymentCreate
   */
  createPayment(payload: CardPaymentCreate): Promise<CardPaymentObject> {
    return this._http.post({
      url: CardPaymentEndpoints.CREATE(this.sandbox),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }
}