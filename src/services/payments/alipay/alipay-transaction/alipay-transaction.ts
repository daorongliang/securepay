import { AlipayEndpoints } from "../../../../constants/alipay-endpoints.const";
import { TokenType, DebugLevel } from "../../../../enums";
import { SecurepayConstruction } from "../../../../interfaces/common/construction.interface";
import { AlipayInitialResponse } from "../../../../interfaces/alipay/alipay-initial-response.interface";
import { AlipayTransactionInitial } from "../../../../interfaces/alipay/alipay-transaction-initial.interface";
import { AlipayTransactionRefund } from "../../../../interfaces/alipay/alipay-transaction-refund.interface";
import { RequestService } from "../../../common/request/request.service";
import { AlipayObject } from "../../../../interfaces/alipay/alipay-object.interface";

export class AlipayTransactionService {

  /** Services */
  private _http: RequestService;

  /** Variables */
  private sandbox      : boolean;
  private debugLevel   : DebugLevel;
  private clientId     : string;
  private clientSecret : string;
  private merchantCode : string;
  
  constructor(options: SecurepayConstruction) {
    this._http        = new RequestService(options);
    this.sandbox      = options.sandbox    || false;
    this.debugLevel   = options.debugLevel || DebugLevel.NONE;
    this.clientId     = options.clientId;
    this.clientSecret = options.clientSecret;
    this.merchantCode = options.merchantCode;
  }

  /**
   * Initiates Alipay transaction.
   * 
   * @param {AlipayTransactionInitial} payload
   */
  initialTransaction(payload: AlipayTransactionInitial): Promise<AlipayInitialResponse> {
    if (!payload.merchantCode)
      payload.merchantCode = this.merchantCode;
    return this._http.post({
      url: AlipayEndpoints.INITIAL(this.sandbox),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }

  /**
   * Refunds a previously executed Alipay transaction
   * 
   * @param {string} orderId
   * @param {AlipayTransactionRefund} payload
   */
  refundTransaction(orderId: string, payload: AlipayTransactionRefund) {
    if (!payload.merchantCode)
      payload.merchantCode = this.merchantCode;
    return this._http.post({
      url: AlipayEndpoints.REFUND(this.sandbox, orderId),
      token_type: TokenType.SECUREPAY_JWT,
      data: payload
    })
  }

  /**
   * Retrieves billing & shipping details for a customer that has previously initiated/executed Alipay transaction
   * 
   * @param {string} orderId
   * @param {string} merchantCode
   */
  retrieveTransaction(orderId: string, merchantCode?: string): Promise<AlipayObject> {
    if (!merchantCode)
        merchantCode = this.merchantCode;
    return this._http.get({
      url: AlipayEndpoints.RETRIEVE(this.sandbox, orderId, merchantCode),
      token_type: TokenType.SECUREPAY_JWT
    })
  }
}