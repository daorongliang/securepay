import { SecurepayConstruction } from "../interfaces/common/construction.interface";
import { PaypalTransactionExecute } from "../interfaces/paypal/paypal-transaction-execute.interface";
import { PaypalTransactionInitial } from "../interfaces/paypal/paypal-transaction-initial.interface";
import { PaypalTransactionRefund } from "../interfaces/paypal/paypal-transaction-refund.interface";
import { PaypalTransactionService } from "../services/payments/paypal/paypal-transaction/paypal-transaction";

export class Paypal {

  /** Services */
  private _paypalTransaction: PaypalTransactionService

  constructor(
    options: SecurepayConstruction
  ) {
    this._paypalTransaction = new PaypalTransactionService(options);
  }

  /**
   * Initiates a PayPal express checkout transaction.
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-paypal-payments-rest-api-initiate-paypal-transaction
   * 
   * @param {PaypalTransactionInitial} payload
   */
  initialTransaction(payload: PaypalTransactionInitial) {
    return this._paypalTransaction.initialTransaction(payload);
  }

  /**
   * Executes a PayPal express checkout transaction after the customer has logged into PayPal and accepted it.
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-paypal-payments-rest-api-execute-paypal-transaction
   * 
   * @param {string} orderId
   * @param {PaypalTransactionExecute} payload
   */
  executeTransaction(orderId: string, payload: PaypalTransactionExecute) {
    return this._paypalTransaction.executeTransaction(orderId, payload);
  }

  /**
   * Refunds a previously executed PayPal transaction
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-paypal-payments-rest-api-refund-paypal-transaction
   * 
   * @param {string} orderId
   * @param {PaypalTransactionRefund} payload
   */
  refundTransaction(orderId: string, payload: PaypalTransactionRefund) {
    return this._paypalTransaction.refundTransaction(orderId, payload);
  }

  /**
   * Retrieves billing & shipping details for a customer that has previously initiated/executed a PayPal transaction
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-paypal-payments-rest-api-retrieve-paypal-order-details
   * 
   * @param {string} orderId
   * @param {string} merchantCode
   */
  retrieveTransaction(orderId: string, merchantCode: string) {
    return this._paypalTransaction.retrieveTransaction(orderId, merchantCode);
  }
}