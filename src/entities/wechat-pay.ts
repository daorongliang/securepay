import { WechatPayTransactionInitial } from "../interfaces/wechat-pay/wechat-pay-transaction-initial.interface";
import { WechatPayTransactionRefund } from "../interfaces/wechat-pay/wechat-pay-transaction-refund.interface";
import { SecurepayConstruction } from "../interfaces/common/construction.interface";
import { WechatPayTransactionService } from "../services/payments/wechat-pay/wechat-pay-transaction/wechat-pay-transaction";

export class WechatPay {

  /** Services */
  private _wechatPayTransaction: WechatPayTransactionService

  constructor(
    options: SecurepayConstruction
  ) {
    this._wechatPayTransaction = new WechatPayTransactionService(options);
  }

  /**
   * Initiates an WechatPay transaction
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-wechat-pay-payments-rest-api-initiate-wechat-pay-transaction
   * 
   * @param {WechatPayTransactionInitial} payload
   */
  initialTransaction(payload: WechatPayTransactionInitial) {
    return this._wechatPayTransaction.initialTransaction(payload);
  }

  /**
   * Used to refund a previous successful WechatPay payment
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-wechat-pay-payments-rest-api-refund-wechat-pay-payment
   * 
   * @param {string} orderId
   * @param {WechatPayTransactionRefund} payload
   */
  refundTransaction(orderId: string, payload: WechatPayTransactionRefund) {
    return this._wechatPayTransaction.refundTransaction(orderId, payload);
  }

  /**
   * Retrieve WechatPay order details
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-wechat-pay-payments-rest-api-retrieve-wechat-pay-order-details
   * 
   * @param {string} orderId
   * @param {string} merchantCode
   */
  retrieveTransaction(orderId: string, merchantCode: string) {
    return this._wechatPayTransaction.retrieveTransaction(orderId, merchantCode);
  }
}