import { DebugLevel } from "../enums/debug-level.enum";
import { CardPaymentCreate } from "../interfaces/card-payment/card-payment-create.interface";
import { CardPaymentInstrumentRequest } from "../interfaces/card-payment/card-payment-instrument-request.interface";
import { SecurepayConstruction } from "../interfaces/common/construction.interface";
import { CardPaymentInstrumentsService } from "../services/payments/card-payments/card-payment-instruments/card-payment-instruments.service";
import { CardPaymentsService } from "../services/payments/card-payments/card-payments/card-payments.service";

export class CardPayment {
  
  /** Variables */
  private clientId     : string     = "";
  private clientSecret : string     = "";
  private sandbox      : boolean    = true;
  private debugLevel   : DebugLevel = DebugLevel.NONE;
  

  /** Services */
  private _cardPayments: CardPaymentsService;
  private _cardPaymentInstruments: CardPaymentInstrumentsService;

  constructor(options: SecurepayConstruction) {
    this._cardPayments = new CardPaymentsService(options);
    this._cardPaymentInstruments = new CardPaymentInstrumentsService(options);
  }

  /**
  /**
   * Create card payment, documentation:
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-create-payment
   * 
   * @param payload CardPaymentCreate
   */
  createPayment(payload: CardPaymentCreate) {
    return this._cardPayments.createPayment(payload);
  }

  /**
   * Create card payment instrument, documentation:
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-create-payment-instrument
   * 
   * @param payload CardPaymentInstrumentRequest
   */
  createPaymentInstrument(payload: CardPaymentInstrumentRequest) {
    
  }

  /**
   * Retrieves stored payment instruments from the vault for an identified customer
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-payment-instruments
   * 
   * @param payload CardPaymentInstrumentRequest
   */
  getPaymentInstruction(payload: CardPaymentInstrumentRequest) {

  }

  /**
   * Deletes a previously stored payment instrument from the vault.
   * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-delete-payment-instrument
   * 
   * @param payload CardPaymentCreate
   */
  deletePaymentInstruction(payload: CardPaymentInstrumentRequest) {
    
  }
}