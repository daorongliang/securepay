import { AlipayTransactionInitial } from "../interfaces/alipay/alipay-transaction-initial.interface";
import { AlipayTransactionRefund } from "../interfaces/alipay/alipay-transaction-refund.interface";
import { SecurepayConstruction } from "../interfaces/common/construction.interface";
import { AlipayTransactionService } from "../services/payments/alipay/alipay-transaction/alipay-transaction";

export class Alipay {

  /** Services */
  private _alipayTransaction: AlipayTransactionService

  constructor(
    options: SecurepayConstruction
  ) {
    this._alipayTransaction = new AlipayTransactionService(options);
  }

  /**
   * Initiates an Alipay transaction
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-alipay-payments-rest-api-initiate-alipay-transaction
   * 
   * @param {AlipayTransactionInitial} payload
   */
  initialTransaction(payload: AlipayTransactionInitial) {
    return this._alipayTransaction.initialTransaction(payload);
  }

  /**
   * Used to refund a previous successful Alipay payment
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-alipay-payments-rest-api-refund-alipay-payment
   * 
   * @param {string} orderId
   * @param {AlipayTransactionRefund} payload
   */
  refundTransaction(orderId: string, payload: AlipayTransactionRefund) {
    return this._alipayTransaction.refundTransaction(orderId, payload);
  }

  /**
   * Retrieve Alipay order details
   * https://auspost.com.au/payments/docs/securepay/#securepay-api-alipay-payments-rest-api-retrieve-alipay-order-details
   * 
   * @param {string} orderId
   * @param {string} merchantCode
   */
  retrieveTransaction(orderId: string, merchantCode: string) {
    return this._alipayTransaction.retrieveTransaction(orderId, merchantCode);
  }
}