import { PaymentStatus } from "../../enums/payment-status.enum";

export interface CardPaymentObject {
  /**
   * A timestamp when transaction was created, in ISO Date Time format with offset from UTC/Greenwich e.g. 2021-07-23T13:00:53.128+10:00.
   */
  createdAt: string,

  /**
   * Merchant account for which the funds are collected
   */
  merchantCode: string,

  /**
   * The identifier for the customer. In case of anonymous payment it is always anonymous.
   */
  customerCode: string,

  /**
   * The tokenised payment instrument that was used.
   */
  token: string,

  /**
   * Client IP address
   */
  ip: string,

  /**
   * Total amount in cents that was charged to the tokenised payment instrument.
   */
  amount: number,

  /**
   * Payment currency.
   */
  currency: string,

  /**
   * The status of the payment. Valid values are paid failed unknown. If the payment was processed and succeeded the status field in payload response is set to paid. If payment was processed but was declined the status is set to failed and errorCode field is populated with error code related to reason of decline. If payment was processed with unexpected status from gateway the status is set to unknown and errorCode field is populated with error code related to reason.
   */
  status: PaymentStatus,

  /**
   * A client order id, will be used as reference to the payment.
   */
  orderId: string,

  /**
   * The payment transaction reference from the payment gateway
   */
  bankTransactionId: string,

  /**
   * Bank response code which identifies the reason the transaction was approved or decline. Refer to bank response code for card payments
   */
  gatewayResponseCode: string,

  /**
   * Detailed message of the bank response code.
   */
  gatewayResponseMessage: string

  /**
   * If transaction was processed but declined by the bank or payment was declined due to results of requested FraudGuard check this field is populated with error code representing reason of failure
   */
  errorCode?: string

  /**
   * If payment request included FraudGuard check this field is populated with FRAUD_GUARD value.
   */
  fraudCheckType?: string

  /**
   * If payment request included FraudGuard check this field is populated fraud check result. Refer to Fraud Check Result for more details.
   */
  fraudCheckResult: {
    /**
     * The fraud check reference number.
     */
    providerReferenceNumber: string,
    /**
     * The result of the fraud check.
     */
    score: number,
    /**
     * This field contains details of the result.
     */
    providerResponseMessage: string
  }
}