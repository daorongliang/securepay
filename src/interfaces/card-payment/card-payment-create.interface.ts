export interface CardPaymentCreate {
  /**
   * An integer value greater than 0, for AUD payments representing the total amount in cents to charge the provided (tokenised) payment instrument. For dynamic currency conversion payments this field should be set to the value of dccQuote.amount field provided in tokenise card object.
   */
  amount: number, 

  /**
   * Merchant account for which the funds are collected.
   */
  merchantCode: string,
  
  /**
   * A tokenised payment instrument reference. This value is used by the payment gateway to retrieve the actual card information, which is then used to perform the transaction.
   */
  token: string, 

  /**
   * A customer IP address. Must be a valid IPv4 or IPv6.
   */
  ip: string, 

  /**
   * A client order id, will be used as reference to the payment. If not provided, SecurePay API will assign a unique id for the order.
   */
  orderId?: string

  /**
   * Payment currency. Default value is AUD. Non AUD payments are supported for dynamic currency conversion payments only. For dynamic currency conversion payments this field should be set to the value of dccQuote.currency field provided in tokenise card object.
   */
   currency?: string

   /**
    * A unique (within your organisation) identifier of your customer. Should not be longer than 30 characters. This is used when you want to perform a payment against a stored payment instrument. Please note anonymous is a reserved keyword and must not be used.
    */
   customerCode?: string

   /**
    * A payment fraud check details object.
    * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-payment-objects-payment-fraud-check-details-object
    */
    fraudCheckDetails?: string

    /**
     * A dynamic currency conversion details object. Should be present for dynamic currency conversion payments.
     * https://auspost.com.au/payments/docs/securepay/?javascript#securepay-api-card-payments-rest-api-payment-objects-dynamic-currency-conversion-details-object
     */
    dccDetails?: {
      /**
       * Should be passed for dynamic currency conversion payments. Should be populated with dynamic conversion orderId returned in Payment Order Object
       */
      initiatedOrderId: string
    }
}