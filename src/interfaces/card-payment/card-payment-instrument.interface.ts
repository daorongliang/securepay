export interface CardPaymentInstrument {
  /**
   * A timestamp when payment instrument was created, in ISO Date Time format with offset from UTC/Greenwich e.g. 2021-07-23T13:00:53.128+10:00.
   */
  createdAt: string,

  /**
   * A unique (within your organisation) identifier of your customer
   */
  customerCode: string,

  /**
   * A tokenised payment instrument reference. This value is used by the payment gateway to retrieve the actual card information, which is then used to perform transactions. 
   * Note: The token is same as the one passed in the request header
   */
  token: string,

  /**
   * Brand of payment instrument. E.g. credit, debit, charge card etc. Refer to Bin base lookup for more details.
   */
  brandType: string,

  /**
   * Category of payment instrument. E.g. standard, premium, business etc. Refer to Bin base lookup for more details
   */
  brandCategory: string,

  /**
   * A card scheme. e.g. visa, mastercard, diners, amex.. If the card scheme card type is not known to SecurePay API unknown value will be returned.
   */
  scheme: string,

  /**
   * Bank identification number. i.e. The first 6 digit numbers of the card.
   * Note: Currently not supported. The field was added for future use.
   */
  bin: string,

  /**
   * The last 4 digits of the card number. (Please note the number of digits returned may vary due to the card scheme, eg: 3 digits may be returned)
   */
  last4: string,

  /**
   * Two digit number representing the card expiry month.
   */
  expiryMonth: string,

  /**
   * wo digit number representing the card expiry year.
   */
  expiryYear: string
}