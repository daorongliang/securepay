export interface CardPaymentInstrumentRequest {
  /**
   * A unique (within your organisation) identifier of your customer. Should not be longer than 30 characters. Please note anonymous is a reserved keyword and must not be used.
   */
  customerCode: string,

  /**
   * A customer IP address. Must be a valid IPv4 or IPv6.
   */
  ip: string

  /**
   * A tokenised payment instrument reference. This value is used by the payment gateway to retrieve the actual card information, which is then used to perform the transaction.
   * 
   * NOTE: Only for create request!!!
   */
  token?: string

}