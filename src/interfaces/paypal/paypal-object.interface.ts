import { PaymentStatus } from "../../index"
import { PaypalPayerDetails } from "./paypal-payer-details.interface"
import { PaypalShippingAddress } from "./paypal-shipping-address.interface"

export interface PaypalObject {
  orderId         : string,
  paymentId       : string,
  status          : PaymentStatus,
  amount          : number,
  payerDetails    : PaypalPayerDetails,
  shippingAddress : PaypalShippingAddress
}