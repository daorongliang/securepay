export interface PaypalTransactionExecute {
  amount      : number, 
  merchantCode: string, 
  ip          : string,
  payerId     : string
}