export interface PaypalTransactionInitial {
  amount             : number, 
  merchantCode      ?: string, 
  ip                 : string, 
  orderId           ?: string,
  paymentType       ?: 'sale',
  noShipping        ?: boolean,
  billingDescription?: string
  redirectUrls       : {
    successUrl : string
    cancelUrl  : string
  }
}