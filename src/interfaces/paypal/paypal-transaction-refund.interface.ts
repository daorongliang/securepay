export interface PaypalTransactionRefund {
  amount        : number, 
  merchantCode ?: string, 
  ip            : string
}