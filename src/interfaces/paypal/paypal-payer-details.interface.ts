export interface PaypalPayerDetails {
  firstName : string,
  lastName  : string,
  email     : string
}