export interface PaypalShippingAddress {
  name        : string,
  streetLine1 : string,
  city        : string,
  stateCode   : string,
  postcode    : string,
  countryCode : string
}