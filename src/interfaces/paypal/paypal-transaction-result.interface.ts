import { PaymentStatus } from "../../index"

export interface PaypalTransactionResult {
  createdAt                : string,
  merchantCode             : string,
  customerCode             : string,
  ip                       : string,
  amount                   : number,
  status                   : PaymentStatus,
  orderId                  : string,
  providerReferenceNumber  : string
  errorCode               ?: number
}