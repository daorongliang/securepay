export interface PaypalInitialResponse {
  createdAt     : string,
  merchantCode  : string,
  customerCode  : string,
  ip            : string,
  amount        : number,
  paymentUrl    : string
  paymentId     : string,
  orderId       : string,
  paymentType   : 'sale',
  noShipping    : boolean
}