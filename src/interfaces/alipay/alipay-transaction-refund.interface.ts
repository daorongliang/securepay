export interface AlipayTransactionRefund {
  merchantCode: string,  
  amount      : number, 
  ip          : string
}
