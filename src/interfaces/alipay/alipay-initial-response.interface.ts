import { AlipayTransactionStatus } from "../../enums/alipay-transaction-status.enum";
import { PaymentChanel } from "../../enums/payment-chanel.enum";

export interface AlipayInitialResponse {
  createdAt               : string,
  merchantCode            : string,
  customerCode            : string,
  ip                      : string,
  amount                  : number,
  orderId                 : string,
  status                  : AlipayTransactionStatus,
  paymentChannel          : PaymentChanel,
  paymentUrl              : string,
  qrCodeImage             : string,
  providerReferenceNumber : string
}