import { PaymentChanel } from "../../enums/payment-chanel.enum";

export interface AlipayTransactionInitial {
  merchantCode     ?: string, 
  ip                : string,
  amount            : number, 
  orderId           : string,
  orderDescription  : string,
  paymentChannel    : PaymentChanel,
  redirectUrls      : {
      successUrl: string
  }
}