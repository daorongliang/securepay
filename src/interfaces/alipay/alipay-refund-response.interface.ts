import { PaymentStatus } from "../../enums/payment-status.enum";

export interface AlipayRefundResponse {
  createdAt               : string,
  merchantCode            : string,
  customerCode            : string,
  ip                      : string,
  amount                  : number,
  orderId                 : string,
  providerReferenceNumber : string,
  status                  : PaymentStatus
}