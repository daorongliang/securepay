import { DebugLevel } from "../../enums/debug-level.enum";

export interface SecurepayConstruction {
  clientId     : string,
  clientSecret : string,
  merchantCode : string,
  sandbox     ?: boolean,
  debugLevel  ?: DebugLevel
}