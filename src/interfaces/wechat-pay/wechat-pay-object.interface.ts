import { PaymentStatus } from "../../enums/payment-status.enum";

export interface WechatPayObject {
  createdAt: string,
  amount: number,
  customerCode: string,
  merchantCode: string,
  orderId: string,
  providerReferenceNumber: string,
  status: PaymentStatus
}