import { WechatPayTransactionStatus } from "../../enums/wechat-pay-transaction-status.enum";
import { PaymentChanel } from "../../enums/payment-chanel.enum";

export interface WechatPayInitialResponse {
  createdAt               : string,
  merchantCode            : string,
  customerCode            : string,
  ip                      : string,
  amount                  : number,
  orderId                 : string,
  status                  : WechatPayTransactionStatus,
  paymentChannel          : PaymentChanel,
  paymentUrl              : string,
  qrCodeImage             : string,
  providerReferenceNumber : string
}