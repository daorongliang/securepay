export interface WechatPayTransactionRefund {
  merchantCode: string,  
  amount      : number, 
  ip          : string
}
