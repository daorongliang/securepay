import { Endpoints, SandboxEndpoints } from "../enums/index";

export const WechatPayEndpoints = {
  /**
   * Initial Wechat Pay
   */
  INITIAL: (sandbox: boolean) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/wechat/payments/initiate`,

  /**
   * Refund Wechat Pay
   */
  REFUND: (sandbox: boolean, orderId: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/wechat/orders/${orderId}/refunds`,

  /**
   * Retrieve Wechat Pay
   */
  RETRIEVE: (sandbox: boolean, orderId: string, merchantCode: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/wechat/orders/${orderId}?merchantCode=${merchantCode}`,
}