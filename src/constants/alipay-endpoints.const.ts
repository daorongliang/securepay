import { Endpoints, SandboxEndpoints } from "../enums/index";

export const AlipayEndpoints = {
  /**
   * Initial Alipay
   */
  INITIAL: (sandbox: boolean) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/alipay/payments/initiate`,

  /**
   * Refund Alipay
   */
  REFUND: (sandbox: boolean, orderId: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/alipay/orders/${orderId}/refunds`,

  /**
   * Retrieve Alipay
   */
  RETRIEVE: (sandbox: boolean, orderId: string, merchantCode: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/alipay/orders/${orderId}?merchantCode=${merchantCode}`,
}