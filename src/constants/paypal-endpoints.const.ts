import { Endpoints, SandboxEndpoints } from "../enums/index";

export const PaypalEndpoints = {
  /**
   * Initial Paypal
   */
  INITIAL: (sandbox: boolean) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/paypal/payments/initiate`,

  /**
   * Execute Paypal
   */
  EXECUTE: (sandbox: boolean, orderId: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/paypal/payments/orders/${orderId}/execute`,

  /**
   * Refund Paypal
   */
  REFUND: (sandbox: boolean, orderId: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/paypal/orders/${orderId}/refunds`,

  /**
   * Retrieve Paypal
   */
  RETRIEVE: (sandbox: boolean, orderId: string, merchantCode: string) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/wallets/paypal/orders/${orderId}?merchantCode=${merchantCode}`,
}