import { Endpoints, SandboxEndpoints } from "../enums/index";

export const CardPaymentEndpoints = {
  /**
   * Initial Paypal
   */
  CREATE: (sandbox: boolean) => `${sandbox ? SandboxEndpoints.BASE_URL : Endpoints.BASE_URL}/v2/payments/account-verification`,
}